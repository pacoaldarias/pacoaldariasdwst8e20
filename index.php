<?php

include_once("controlador/funciones.php");
include_once("modelo/Persona.php");

$ctrl = recoge('ctrl');
$nombre = recoge('nombre');
$persona = new Persona();
$persona->setNombre($nombre);

if ($ctrl == "") {
   include_once("vista/inicio.php");
}
if ($ctrl == "entrada") {
   include_once("vista/entrada.php");
}
if ($ctrl == "salida") {
   include_once("vista/salida.php");
}
?>
