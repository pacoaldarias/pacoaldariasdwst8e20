<html>
    <head>
        <meta charset="UTF-8">
        <title>MVC</title>
    </head>
    <body>
        <h1>Ejemplo Simple MVC</h1>
        <h2>Inicio</h2>

        <ul>
            <li><a href="index.php?ctrl=entrada">Entrada.</li>
            <li><a href="index.php?ctrl=salida&nombre=<?php echo $persona->getNombre(); ?>">Salida.</a></li>
            <li><a href="vista/explica.html">Explicación.</a></li>
        </ul>

        <hr>
        <p>Por Paco Aldarias </p>
    </body>
</html>

